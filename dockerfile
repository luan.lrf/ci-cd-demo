FROM httpd:latest

WORKDIR /usr/local/apache2/htdocs/

COPY ./app/ /usr/local/apache2/htdocs/

EXPOSE 80   